"""Entry point module: process-chunk

Implements the entry-point by using Python or any other languages.
"""

import os
import re

from diva_evaluation_cli.src.implementation.process_chunk import process_chunk

def entry_point(chunk_id, system_cache_dir):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Process a chunk.

    Args:
        chunk_id (int): Chunk id
        system_cache_dir (str): Path to system cache directory

    """
    # raise NotImplementedError("You should implement the entry_point method.")
    # get the int inside the id
    if isinstance(chunk_id, int):
        if chunk_id != 1:
            return
    if isinstance(chunk_id, str):
        chunk_key = re.sub('Chunk','', chunk_id)
        if int(chunk_key) != 1:
            return

    # go into the right directory to execute the script
    # path = os.path.dirname(os.path.abspath(__file__))
    chunk_path = os.path.join(system_cache_dir, 'data_mcprl/chunk.json')
    # path = os.path.join(path, '../data_mcprl/chunk.json')
    assert os.path.exists(chunk_path), "Chunk_json : {} does not exit !!!".format(chunk_path)

    file_index_path = os.path.join(system_cache_dir, 'data_mcprl/file_index.json')
    assert os.path.exists(chunk_path), "File_json : {} does not exit !!!".format(file_index_path)


    # path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(system_cache_dir, 'data_mcprl/video_location.txt')
    # path = os.path.join(path, '../data_mcprl/video_location.txt')
    assert os.path.exists(path), "{} does not exit !!!".format(path)
    video_location = ""
    with open(os.path.join(path), 'r') as f:
        video_location = f.readline()
    assert len(video_location), "video_location : {} is empty!".format(video_location)
    print(file_index_path)
    process_chunk(chunk_path, video_location, file_index_path)
