
import os
from multiprocessing import Pool, Queue, Manager
import json
import pynvml
from pprint import pprint

indoor_cams = ["G299", "G326", "G329", "G330", "G331", "G419", "G420", "G421", "G423", "G508"]
indoor_cams = []

def docker_worker(args):
    q, video_path, video_name, res_save = args
    pprint(args)
    gpu_id = q.get()
    print('video_path ', video_path)
    print('video_name ', video_name)
    if not os.path.exists(video_path):
        print('Warning: ', video_path, ' does not exits')
        return

    cmd = 'nvidia-docker run --shm-size=16g -it --rm \
            -v {}:/home/meva/meva_video/{} \
            -v {}:/home/CHUNK_RESULT \
            840989026 \
            /bin/bash -c "/home/run.sh {} {} {}"'.format(video_path, video_name, res_save, video_name, video_name, gpu_id)
    print(cmd)
    os.system(cmd)
    '''
    cq_video_path = os.path.join(video_location, video_path)
    cq_cmd = 'nvidia-docker run --shm-size=16g -it --rm \
              -v {}:/home/video/{}  \
              -v {}:/home/data/ \
              -v {}:/home/json/ \
              actev  \
              /bin/bash -c "cd /home/FairMOT && bash run.sh {} {}"'.format(cq_video_path, video_name,
                                                                           video_frame, json_path, gpu_id, video_name)

    cq_cmd2 = 'nvidia-docker run --shm-size=16g -it --rm \
               -v {}:/home/meva/meva_frames/ \
               -v {}:/home/result/ \
               -v {}:/home/CHUNK_RESULT \
               actev_0712 \
               /bin/bash -c "cd /home/cq_eco && bash caoqun_test.sh {}"'.format(video_frame, json_path, res_save, gpu_id)

    video_cam = video_name.split('.')[-2]
    print(video_cam)
    if video_cam not in indoor_cams:
        print(cmd)
        os.system(cmd)
    else:
        os.system(cq_cmd)
        os.system(cq_cmd2)
    '''
    q.put(gpu_id)

def load_json(json_file):
    with open(json_file, 'r') as f:
        json_data = json.load(f)
    return json_data


def get_name_path_dict(video_location):
    name_path_dict = {}
    for path, _, video_names in os.walk(video_location):
        for video_name in video_names:
            if not  video_name.endswith('.avi'):
                continue
            name_path_dict[video_name] = os.path.join(path, video_name)
    return name_path_dict


def process_chunk(jsonpath, video_location, file_index_json):
    files = None
    with open(jsonpath, 'r') as f:
        chunk = json.load(f)
        assert len(list(chunk.keys())) == 1, "chunk number should be 1 !"
        key = list(chunk.keys())[0]
        files = list(chunk[key]["files"])
    assert files is not None, "files to be processed should not be None!"
    res_save = os.path.abspath(os.path.dirname(os.path.abspath(jsonpath)))
    frame_cache = os.path.join(res_save, "video_frame_dir")
    cq_json_path = os.path.join(res_save, "person_track_json")
    res_save = os.path.join(res_save, "CHUNK_RES")

    if os.path.exists(res_save):
        os.system("rm -rf {}".format(res_save))
    print('res_save: ', res_save)
    os.makedirs(res_save)
    # os.makedirs(frame_cache)
    # os.makedirs(cq_json_path)
    
    # file_json = load_json(file_index_json)
    # pprint(file_json)
    name_path_dict = get_name_path_dict(video_location)
    pprint(name_path_dict)
    pynvml.nvmlInit()
    gpu_num = pynvml.nvmlDeviceGetCount()
    manager = Manager()
    q = manager.Queue()
    for i in range(gpu_num):
    # for i in [3, 4, 5, 6]:
        q.put(i)
    
    pool = Pool(gpu_num)
    # args = [(q, video_location, file_json[video_name]['filename'], video_name,
    #         res_save, frame_cache, cq_json_path) for video_name in files]

    args = [(q, name_path_dict[video_name], video_name,
             res_save) for video_name in files]
    pool.map(docker_worker, args)
