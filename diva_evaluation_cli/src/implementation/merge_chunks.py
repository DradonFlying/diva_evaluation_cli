"""Merge chunk source code

"""
import json
import os
import re

def check_format(file, aod=False):
    '''

    :param file: file : type of str ,the path of the *json file needed to be checked
    :return: 
    '''
    print("checking file: {}".format(file))
    with open(file, "r") as f:
        json_file = json.load(f)
    assert "filesProcessed" in json_file.keys(), "filesProcessed not exist"
    assert isinstance(json_file["filesProcessed"], list), "filesProcessed should be a list"
    assert len(json_file["filesProcessed"]), "length of filesProcessed"

    assert "activities" in json_file.keys(), "not exist activities"
    assert isinstance(json_file["activities"], list), "activities should be a list"

    for act in json_file["activities"]:
        assert "activity" in act.keys(), "activity not exist"
        assert isinstance(act["activity"], str), "activity should be a str"

        assert "activityID" in act.keys(), "activityID not exist"
        assert isinstance(act["activityID"], int), "activityID should be a int"

        assert "presenceConf" in act.keys(), "presenceConf not exist"
        assert isinstance(act["presenceConf"], float), "presenceConf should be a float"

        assert "alertFrame" in act.keys(), "alertFrame not exist"
        assert isinstance(act["alertFrame"], int), "alertFrame should be a int"

        assert "localization" in act.keys(), "localization not exist"
        name = list(act["localization"].keys())[0]
        for k in act["localization"][name].keys():
            assert isinstance(act["localization"][name][k], int), "start/end frame should be a int"

        if not aod:
            continue

        assert "objects" in act.keys(), "objects not in exist"
        assert isinstance(act["objects"], list), "objects should be a list"

        for ob in act["objects"]:
            assert "objectType" in ob.keys(), "objectType not exist"
            assert isinstance(ob["objectType"], str), "objectType should be a str"

            assert "objectID" in ob.keys(), "objectID not exist"
            assert isinstance(ob["objectID"], int), "objectID should be a int"

            assert "localization" in ob.keys(), "localization not exist"
            assert isinstance(ob["localization"], dict), "localization should be a dict"

            assert name in ob["localization"], "{} not exit in ob[\"localization\"]".format(name)
            assert isinstance(ob["localization"][name], dict), "{} should be a dict".format(name)

            for i, k in enumerate(sorted(ob["localization"][name].keys())):
                assert isinstance(ob["localization"][name][k], dict), "frmae {} should be a float".format(k)
                if len(ob["localization"][name][k].keys()) == 0:
                    continue

                assert "presenceConf" in ob["localization"][name][
                    k], "presenceConf not exist ob[\"localization\"][name]"
                assert isinstance(ob["localization"][name][k]["presenceConf"], float), "presenceConf should be a float"

                assert "boundingBox" in ob["localization"][name][k], "boundingBox not exist ob[\"localization\"][name]"
                assert isinstance(ob["localization"][name][k]["boundingBox"], dict), "boundingBox should be a dict"

                assert "x" in ob["localization"][name][k][
                    "boundingBox"], "x not exist ob[\"localization\"][name][\"boundingBox\"]"
                assert isinstance(ob["localization"][name][k]["boundingBox"]["x"], int), "boundingBox-x should be a int"

                assert "y" in ob["localization"][name][k][
                    "boundingBox"], "y not exist ob[\"localization\"][name][\"boundingBox\"]"
                assert isinstance(ob["localization"][name][k]["boundingBox"]["y"], int), "boundingBox-y should be a int"

                assert "w" in ob["localization"][name][k][
                    "boundingBox"], "w not exist ob[\"localization\"][name][\"boundingBox\"]"
                assert isinstance(ob["localization"][name][k]["boundingBox"]["w"], int), "boundingBox-w should be a int"

                assert "h" in ob["localization"][name][k][
                    "boundingBox"], "h not exist ob[\"localization\"][name][\"boundingBox\"]"
                assert isinstance(ob["localization"][name][k]["boundingBox"]["h"], int), "boundingBox-h should be a int"


def merge_jsons(json_path):
    final_d = {"filesProcessed": [], "activities": []}
    for x in os.listdir(json_path):
        check_format(os.path.join(json_path, x))
        print("merging " + x)
        with open(os.path.join(json_path, x), 'r') as f:
            d = json.load(f)
            final_d["filesProcessed"].extend(d["filesProcessed"])
            final_d["activities"].extend(d["activities"])
        final_d["filesProcessed"] = list(set(final_d["filesProcessed"]))
    for i, act in enumerate(final_d["activities"]):
        act["activityID"] = i

    
    return final_d


def merge_chunks(result_location, output_file, chunks_file, chunk_ids=None):
    """Merge a list of chunk results files inside an output file, and a chunks file.

    Args:
        result_location (str): Path to chunk results files
        output_file (str): Path to output file where chunks are merged
        chunks_file (str): Path to a global chunk file created after merging
        chunk_ids (:obj: `list`, optional): List of chunk ids to merge, if empty, merge every chunk results files

    """
    path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
    path = os.path.join(result_location, 'data_mcprl/CHUNK_RES/')
    assert os.path.exists(path) and os.path.isdir(path), "chunk result : {} does not exit !!!".format(path)
    
    final_d = merge_jsons(path)
    with open(output_file, 'w') as f:
        json.dump(final_d, f, indent=2)

    chunks_dict = {}
    chunks_dict["Chunk1"] = {}
    chunks_dict["Chunk1"].update({"activities": final_d["activities"], "files": final_d["filesProcessed"]})

    with open(chunks_file, 'w') as f:
        json.dump(chunks_dict, f, indent=2)
