"""Design chunks source code

"""
import json
import os

def design_chunks(file_index_file, activity_index_file, save_path, num_videos_per_chunk):
    """  Merge file_index with activity_index content into different chunks written in a file

    Args:
        file_index_file (str):       Path to file index json file
        activity_index_file (str):   Path to activity index json file
        save_path (str):             Path to save chunks file
        num_videos_per_chunks(int):  number of videos in a chunk
    """
    file_index = json.load(open(file_index_file, 'r'))
    activity_index = json.load(open(activity_index_file, 'r'))
    chunk_dict = {}
    chunk_count = 0
    chunk_prefix = "Chunk"
    all_activities = list(activity_index.keys())

    for index, file_name in enumerate(file_index.keys()):
        if index % num_videos_per_chunk == 0:
            # start a new chunk
            chunk_count += 1
            chunk_name = chunk_prefix + str(chunk_count)
            chunk_dict[chunk_name] = {"activities": all_activities, 
                                      "files": []}
        chunk_name = chunk_prefix + str(chunk_count)
        chunk_dict[chunk_name]["files"].append(file_name)
    assert len(list(chunk_dict.keys())) == 1, "chunk number should be 1 !"
    with open(save_path, 'w') as f:
        json.dump(chunk_dict, f, indent=2)
    
    # path = os.path.dirname(os.path.abspath(__file__))
    # path = os.path.join(path, '../data_mcprl')
    # os.makedirs(path, exist_ok=True)
    # with open(os.path.join(path, "chunk.json"), 'w') as f:
    #     json.dump(chunk_dict, f, indent=2)

